"""Market order simulator."""
from datetime import datetime

BUY = "Buy"
SELL = "Sell"


class MarketSimulator(object):
    """Contains buyer orders, seller orders and required functionality."""

    def __init__(self):
        """Initialize input storage, buy/sell orders and final match objects."""
        self.all_input = []
        self.buy_orders = {}
        self.sell_orders = {}
        self.final_buys = []

    def get_stock_input(self):
        """Take input and start processing orders on the fly, add rest to buyer/seller market."""
        print "Enter the orders, or quit to exit: \n"
        while True:
            order_line = raw_input().strip()
            if order_line not in ["quit", ""]:
                self.all_input.append(order_line.strip())
            else:
                break

        # for each line in input, if a selling stock, add to sale market
        # if a buy stock, get matching stock from sale market and put remaining buy stock (if any)
        # in buy market to process later (and update sale market for stock which has been taken).
        for line in self.all_input:
            order_id, time_string, stock, buy_sell, qty, price = line.split(" ")
            order_id = int(order_id)
            order_time = datetime.strptime(time_string, '%H:%M')
            price = float(price)
            qty = int(qty)
            if buy_sell == BUY:
                # buy matching sale stock (if any), otherwise add to buy market
                remaining_qty = self.match_stock(stock, qty, price, order_time)
                if remaining_qty > 0:
                    self.save_stock(BUY, order_id, stock, remaining_qty, price, order_time)
            else:
                # add to sale market
                self.save_stock(SELL, order_id, stock, qty, price, order_time)

    def match_stock(self, stock, qty, price, order_time):
        """Get matching stock, and remove qty/ stock from sell market.

        :return: the stock left over so buyer market can be updated with that stock
        """
        if stock not in self.sell_orders:
            # no sell orders yet, add to queue
            return qty

        # list all sale orders by price in increasing order for the given stock
        # the order makes sure lower price ones get picked first
        valid_on_sale_order = sorted(
            filter(lambda x: x[2] < price, self.sell_orders[stock]),
            key=lambda x: (x[2], x[3])  # price - less, time - earlier; lower selling price gets priority
        )

        # reduce the qty of matching stock until buy stock is over, or matching sale stock gets over
        needed_qty = qty
        for each_order in valid_on_sale_order:
            if needed_qty > 0:
                current_sale_qty = each_order[1]

                if current_sale_qty > needed_qty:
                    remove_qty = needed_qty
                else:
                    remove_qty = current_sale_qty

                needed_qty -= remove_qty  # reduce needed qty for next iteration

                # add to final buys where the matches are stored
                self.final_buys.append([str(each_order[0]), str(remove_qty), "%.2f" % each_order[2]])

                # update left over qty of on sale stock
                index_to_delete = None
                for order_index in xrange(len(self.sell_orders[stock])):
                    # fetch the order from sale orders with matching ID, then update the qty in it.
                    if self.sell_orders[stock][order_index][0] == each_order[0]:
                        if current_sale_qty == remove_qty:
                            # all stock over, keep flag to delete this item from sale market
                            index_to_delete = order_index
                        else:
                            # reduce the stock if stock is still left
                            self.sell_orders[stock][order_index][1] = current_sale_qty - remove_qty

                        break

                # remove sale order if all qty is over
                if index_to_delete:
                    del self.sell_orders[stock][index_to_delete]

            else:
                break

        return needed_qty

    def save_stock(self, stock_type, order_id, stock, qty, price, order_time):
        """Save orders in buyer orders or seller orders."""
        if stock_type == BUY:
            container = self.buy_orders
        else:
            container = self.sell_orders

        if stock not in container:
            container[stock] = []

        container[stock].append([order_id, qty, price, order_time])

    def match_remaining_stocks(self):
        """Use price and time based matching to print out the remaining matches from market.

        Buy stocks will be ordered according to their 1. price 2. time (FIFO),
        and matched against sale market to get matches as the last step.
        """
        for stock in self.buy_orders:
            if stock in self.sell_orders:
                # if this stock exists in sellers, then only check matching
                current_buy_orders = sorted(
                    self.buy_orders[stock],
                    # price - higher, time - early; higher buying price gets priority (this can be changed)
                    key=lambda x: (-x[2], x[3])
                )

                for buy_order in current_buy_orders:
                    # no need for return as we don't need this stock anymore
                    self.match_stock(stock, buy_order[1], buy_order[2], buy_order[3])

    def get_final_matches(self):
        """Print the final matches."""
        for each_buy in self.final_buys:
            print ' '.join(each_buy)


if __name__ == "__main__":
    market_simulator = MarketSimulator()
    market_simulator.get_stock_input()
    market_simulator.match_remaining_stocks()
    market_simulator.get_final_matches()
