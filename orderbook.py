from decimal import Decimal


class Trader():
    pass


class Buyer(Trader):
    pass


class Seller(Trader):
    pass


class Order():

    def __init__(self, order_id, ordered_time, stock_name, stock_type, qty, price):
        self.order_id = self.parse_order_id(order_id)
        self.ordered_time = self.parse_ordered_time(ordered_time)
        self.stock_name = self.parse_stock_name(stock_name)
        self.stock_type = self.parse_stock_type(stock_type)
        self.qty = self.parse_qty(qty)
        self.price = self.parse_price(price)

    def parse_order_id(order_id):
        return int(order_id)

    def parse_ordered_time(ordered_time):
        return datetime.strptime(ordered_time, '%H:%M')

    def parse_stock_name(stock_name):
        return stock_name.uppper()

    def parse_stock_type(stock_type):
        return stock_type.upper()

    def parse_qty(qty):
        return int(qty)

    def parse_price(price):
        return Decimal(price)  # TODO: set max decimals here


class OrderBook():
    pass
